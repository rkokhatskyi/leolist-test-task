Feature: Search with filters tests

  @test
  @TestCaseId("T1")
  @Severity(SeverityLevel.CRITICAL)
  @expandedDisplay
  Scenario: QA-T1: Search keyword with filters - All content present
    Given user is on Calgary page
    When user selects the "British Columbia" region
    And user selects the "Metro Vancouver" district
    When user clicks More filter button
    And user selects Has Photos checkbox
    And user selects "Vancouver" check box in Cities filter
    And user inserts "Scrap Car" text into Search input
    And user clicks Show Ads button
    Then Ads in the result table contain searched text
    And Ads in the result table from selected city only
    And Ads in the result table contain photos