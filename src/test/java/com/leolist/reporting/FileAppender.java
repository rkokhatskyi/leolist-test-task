package com.leolist.reporting;

import com.leolist.utils.Properties;

import java.io.*;

public class FileAppender {

    private static final String reportsDirPath = Properties.getReportsDirPath();

    static {
        File file = new File(reportsDirPath);

        //create directory for the files
        if (!(file.exists() || file.mkdirs())) {
            EventLogger.error("Cannot create report/ directory");
        }
    }

    public static void addFailedResult(String message) {
        appendToFile("failedTests.txt", message);
    }

    public static void addResult(String message) {
        appendToFile("allTests.txt", message);
    }

    private static void appendToFile(String fileName, String message) {
        try (FileWriter fileWriter = new FileWriter("report".concat(System.getProperty("file.separator")).concat(fileName), true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             PrintWriter printWriter = new PrintWriter(bufferedWriter, true)) {
            printWriter.println(message);
        } catch (IOException exception) {
            EventLogger.warn("Couldn't write to file - ".concat(fileName));
        }
    }
}