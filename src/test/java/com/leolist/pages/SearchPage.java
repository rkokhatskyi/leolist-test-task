package com.leolist.pages;

import com.leolist.base.TestNGRunner;
import com.leolist.pages.base.BaseWorkspacePage;
import com.leolist.pages.components.filter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.List;

import static com.leolist.base.DriverManager.driver;

public class SearchPage extends BaseWorkspacePage {

    @FindBy(xpath = "//*[starts-with(@id,'item')]/a[2]/div/div[1]/div")
    private List<WebElement> searchResultsTitleLabels;

    @FindBy(xpath = "//*[starts-with(@id,'item')]/a[2]//span[1]")
    private List<WebElement> searchResultsCitiesLabels;

    @FindBy(xpath = "//*[starts-with(@id,'item')]//*[@class='img']/img[1]")
    private List<WebElement> searchResultsPhotos;

    public filter filter;

    public SearchPage() {
        filter = new filter();
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver())), this);
    }

    public List<WebElement> getSearchResultsTitleLabels() {
        if (searchResultsTitleLabels.isEmpty()) {
            TestNGRunner.skipTest("No titles found for results");
        }

        return searchResultsTitleLabels;
    }

    public List<WebElement> getSearchResultsCitiesLabels() {
        if (searchResultsCitiesLabels.isEmpty()) {
            TestNGRunner.skipTest("No cities found for results");
        }

        return searchResultsCitiesLabels;
    }

    public List<WebElement> getSearchResultsPhotos() {
        if (searchResultsPhotos.isEmpty()) {
            TestNGRunner.skipTest("No photos found for results");
        }

        return searchResultsPhotos;
    }
}