package com.leolist.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import static com.leolist.base.DriverManager.driver;

/**
 * Class with custom expected conditions for WebDriverWait
 */
public class AdditionalConditions {

    public static ExpectedCondition<Boolean> pageLoaded(String pagePartialUrl) {
        return driver -> (driver().executeScript("return window.location.href").toString().contains(pagePartialUrl) &&
                driver().executeScript("return document.readyState").equals("complete"));
    }

    public static ExpectedCondition<Boolean> elementIsNotVisible(WebElement element) {
        return driver -> !element.isDisplayed();
    }
}