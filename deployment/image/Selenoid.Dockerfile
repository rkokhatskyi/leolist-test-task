# Dockerfile
FROM aerokube/selenoid:1.6.1

MAINTAINER roman.kokhatskyi@gmail.com

#append files
ADD deployment/configs/selenoid /etc/selenoid
