package com.leolist.stepDefinitions;

import com.leolist.pages.SearchPage;
import com.leolist.reporting.EventLogger;
import com.leolist.tests.TestContext;
import com.leolist.tests.sharedVariableKeys;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Objects;

public class filterResultsStepDefinitions {

    private final SearchPage searchPage = new SearchPage();

    @Then("^Ads in the result table contain searched text$")
    public void adsInTheResultTableContainSearchedText() {

        String searchedText = getSearchKeyword();

        for (WebElement element : searchPage.getSearchResultsTitleLabels()) {
//            TODO fails due to the issue
            Assert.assertTrue(element.getText().contains(searchedText));
        }
    }

    @And("^Ads in the result table from selected city only$")
    public void adsInTheResultTableFromSelectedCityOnly() {

        Object object = TestContext.getSharedVariable(sharedVariableKeys.selectedCity);
        if (Objects.isNull(object)) {
            EventLogger.error("Selected city is not defined");
            throw new IllegalArgumentException("Selected city is null");
        }

        String searchedCity = object.toString();

        for (WebElement element : searchPage.getSearchResultsCitiesLabels()) {
            Assert.assertEquals(element.getText().replaceAll("\\s+", ""),
                    "—".concat(searchedCity));
        }
    }

    @And("^Ads in the result table contain photos$")
    public void adsInTheResultTableContainPhotos() {
        String searchKeyword = getSearchKeyword();

        List<WebElement> photos = searchPage.getSearchResultsPhotos();

        //compare that amount of photos is the same as labels/elements
        Assert.assertEquals(searchPage.getSearchResultsTitleLabels().size(), photos.size());

        for (WebElement element : photos) {
            Assert.assertTrue(element.getAttribute("src").endsWith(".png"));

//             TODO fails due to the issue
            Assert.assertTrue(element.getAttribute("alt").contains(searchKeyword));
        }
    }

    private String getSearchKeyword() {
        Object object = TestContext.getSharedVariable(sharedVariableKeys.searchKeyword);
        if (Objects.isNull(object)) {
            EventLogger.error("Search keyword is not defined");
            throw new IllegalArgumentException("Search keyword is null");
        }

        return object.toString();
    }
}