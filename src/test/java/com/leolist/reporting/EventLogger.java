package com.leolist.reporting;

import com.leolist.base.DriverManager;
import com.leolist.utils.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

import static com.leolist.utils.Properties.getConfigDir;

/**
 * Implements event logging logic.
 */
public class EventLogger {

    private static Logger logger;

    static {
        EventLogger.setDefaultLogger();
        EventLogger.applyLogFile();
    }

    public static void saveSeleniumLogs() {

        EventLogger.setSeleniumLogger();

        WebDriver driver = DriverManager.driver();

        if (Objects.nonNull(driver)) {
            for (String logType : driver.manage().logs().getAvailableLogTypes()) {

                info("[LogTYPE]: " + logType + "\n");
                for (LogEntry entry : driver.manage().logs().get(logType)) {
                    debug(entry.toString());
                }
            }
        }

        //return back previous logger
        EventLogger.setDefaultLogger();
    }

    private static void setDefaultLogger() {
        EventLogger.logger = LogManager.getLogger(EventLogger.class);
    }

    private static void addAPILogs(String logs) {
        EventLogger.logger = LogManager.getLogger("Rest-Assured");
        logger.debug(logs);
        setDefaultLogger();
    }

    private static void setSeleniumLogger() {
        EventLogger.logger = LogManager.getLogger("Selenium");
    }

    private static void applyLogFile() {
        // get all system properties to override logfile
        java.util.Properties properties = System.getProperties();

        String logFile = "logfile.log";
        String logsDir = Properties.getLogsDir();

        properties.setProperty("logfile.name", logsDir + logFile);
        System.setProperties(properties);
        PropertyConfigurator.configure(getConfigDir().concat("log4j.properties"));
    }


    /**
     * Logs message on Info level.
     *
     * @param message is message to log.
     */
    public static void info(String message) {
        logger.info(message);
    }

    /**
     * Logs message on Debug level.
     *
     * @param message is message to log.
     */
    public static void debug(String message) {
        logger.debug(message);
    }

    /**
     * Logs error on Debug level.
     *
     * @param exception is exception to log.
     */
    public static void debug(Exception exception) {
        logger.debug(exception.getMessage());
    }

    /**
     * Logs message on Error level.
     *
     * @param message is message to log.
     */
    public static void error(String message) {
        logger.error(message);
    }

    /**
     * Logs error on Error level.
     *
     * @param exception is exception to log.
     */
    public static void error(Exception exception) {
        logger.error(exception.getMessage());
    }

    /**
     * Logs message on Warning level.
     *
     * @param message is message to log.
     */
    public static void warn(String message) {
        logger.warn(message);
    }

    public static void logRequest(ByteArrayOutputStream stream) {
        EventLogger.addAPILogs("Executing API request: \n" + String.valueOf(stream));
    }

    public static void logResponse(ByteArrayOutputStream stream) {
        EventLogger.addAPILogs("Response:\n" + String.valueOf(stream));
    }
}