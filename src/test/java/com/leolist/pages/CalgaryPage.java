package com.leolist.pages;

import com.leolist.base.DriverManager;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class CalgaryPage extends SearchPage {

    private final EventFiringWebDriver driver;

    public CalgaryPage() {
        driver = DriverManager.driver();
    }

    public Boolean isPageDisplayed() {
        return driver.getCurrentUrl().endsWith("/calgary") &&
                driver.getTitle().contains("Calgary");
    }
}