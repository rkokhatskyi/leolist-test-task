package com.leolist.base;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.logging.Level;

class BrowserPresets {

    private static final Platform platform;
    private static final String browserName = BrowserType.CHROME;

    static {
        platform = getPlatform();
    }

    static DesiredCapabilities getChromeDesiredCapabilities() {

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        capabilities.setBrowserName(browserName);
        capabilities.setPlatform(platform);
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

        // apply chrome options to desired capabilities
        ChromeOptions options = getChromePresetOptions();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        LoggingPreferences loggingPreferences = new LoggingPreferences();

        // enable logging for Selenium
        loggingPreferences.enable(LogType.BROWSER, Level.ALL);
        loggingPreferences.enable(LogType.CLIENT, Level.ALL);
        loggingPreferences.enable(LogType.DRIVER, Level.ALL);
        loggingPreferences.enable(LogType.PERFORMANCE, Level.ALL);
        loggingPreferences.enable(LogType.PROFILER, Level.ALL);
        loggingPreferences.enable(LogType.SERVER, Level.ALL);

        return capabilities;
    }

    static ChromeOptions getChromePresetOptions() {

        ChromeOptions options = new ChromeOptions();

        HashMap<String, Object> chromePreferences = new HashMap<>();
        chromePreferences.put("directory_upgrade", true);

        options.addArguments("--disable-extensions");
        options.addArguments("--disable-infobars");

        options.setExperimentalOption("prefs", chromePreferences);

        return options;
    }

    /**
     * Returns platform details on which tests are running.
     */
    private static Platform getPlatform() {
        Platform platform;
        if (SystemUtils.IS_OS_WINDOWS) {
            platform = Platform.WINDOWS;
        } else if (SystemUtils.IS_OS_MAC) {
            platform = Platform.MAC;
        } else {
            platform = Platform.ANY;
        }
        return platform;
    }
}