package com.leolist.runners;

import com.leolist.base.BaseTestRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

/**
 * Empty test run that do nothing. Required to download all libraries in docker build
 */
@CucumberOptions(
        features = "src/test/java/com/leolist/tests",
        glue = {"com.leolist.stepDefinitions", "com.leolist.runners"},
        tags = {"@ignore"},
        strict = true,
        snippets = SnippetType.CAMELCASE,
        monochrome = true,
        plugin = "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"
)
public class EmptyTestRunner extends BaseTestRunner {
}