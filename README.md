## LeoList Java-Selenium-Cucumber Tests

Automated test cases for the LeoList.

## Getting Started

Clone with Git using the [project](https://bitbucket.org/rkokhatskyi/leolist-test-task) web address.

## Prerequisites

Skip this step if you are going to run tests on your local (host) browser.

#### Environment setup

##### 1. Install Minikube

VT-x or AMD-v virtualization must be enabled in your computer’s BIOS.
If you do not already have a hypervisor installed, install one now.
- For OS X, install [xhyve driver](https://git.k8s.io/minikube/docs/drivers.md#xhyve-driver), [VirtualBox](https://www.virtualbox.org/wiki/Downloads), or [VMware Fusion](https://www.vmware.com/products/fusion).
- For Linux, install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) or [KVM](http://www.linux-kvm.org/).
- For Windows, install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) or [Hyper-V](https://msdn.microsoft.com/en-us/virtualization/hyperv_on_windows/quick_start/walkthrough_install).
(We use VirtualBox below)

###### OSX

`$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.24.1/minikube-darwin-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/`

###### Linux

`$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.24.1/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/`

###### Debian Package (.deb) \[Experimental\]

Download the minikube_0.24-1.deb file, and install it using
`$ sudo dpkg -i minikube_.deb`

###### Windows \[Experimental\]

Download the minikube-windows-amd64.exe file from [here](https://github.com/kubernetes/minikube/releases/tag/v0.24.1), rename it to minikube.exe and add it to your path.

###### Windows Installer \[Experimental\]

Download the minikube-installer.exe file from [here](https://github.com/kubernetes/minikube/releases/tag/v0.24.1), and execute the installer. This will automatically add minikube.exe to your path with an uninstaller available as well. 

###### Upgrading Minikube

`$ minikube delete`

`$ sudo rm -rf ~/.minikube`

> // For Windows only delete `.kube` and `.minikube` folders from `C:\Users\%username%`
Then install the new version of Minikube.

##### 2. Install Docker

###### OSX

https://docs.docker.com/docker-for-mac/install/#install-and-run-docker-for-mac

###### Linux

https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
https://docs.docker.com/engine/installation/linux/docker-ce/debian/
https://docs.docker.com/engine/installation/linux/docker-ce/centos/
https://docs.docker.com/engine/installation/linux/docker-ce/fedora/

###### Windows

https://docs.docker.com/docker-for-windows/install/#where-to-go-next


##### 3. Minikube start

`$ minikube start`

If minikube couldn't start due to "Error creating host: Error creating machine: The system cannot find the path specified" note that run cmd (Windows problem) from the same disk drive. https://github.com/kubernetes/minikube/issues/822

##### 4. Configure shared folder

1. Go to VirtualBox Manager
2. 	Open Minikube settings
3. 	Go to Shared Folders
4. 	Click Add new shared folder
5. 	Set Folder path to project-root folder
6. 	Set Folder name `mount-9p`
7. 	Check Auto-mount and Make Permanent checkboxes

##### 5. Restart Minikube 

`$ minikube stop`

`$ minikube start`


##### 6. Setup Docker env 

`$ eval $(minikube docker-env)`

> // For Windows OS only

`$ @FOR /f "tokens=*" %i IN ('minikube docker-env') DO @%i`

##### 7. Pull images 

`$ minikube ssh`

`$ docker pull selenoid/vnc:chrome_65.0`

>  In case you need to renew browser image:

    `$ minikube ssh`

    `$ docker images`

    `REPOSITORY`|`TAG`|`IMAGE ID`|`CREATED`|`SIZE`
    ------------|-----|----------|---------|-----
    `selenoid/vnc`|  `chrome_65.0`|`faaf3f3ce248`|   `8 hours ago`| `742MB`
   
##### 8. Build docker image

`$ docker build --force-rm -t my-leolist-qa:1.0 -f deployment/image/Minikube.Dockerfile .`

> If _"unable to prepare context: unable to evaluate symlinks in Dockerfile path: lstat /home/docker/deployment: no such file or directory"_ error occurred, then go to `mount-9p` folder directly and try again:

`$ cd /mount-9p/`

`$ docker build --force-rm -t my-leolist-qa:1.0 -f deployment/image/Minikube.Dockerfile .`


##### 9. Get Minikube ip

`$ minikube ip`

##### 10. Setup pom.xml

Set hub in `pom.xml` at properties section

    <hub>http://MINIKUBE_IP:NODE_PORT/wd/hub</hub> 

The `NODE_PORT` defined in `deployment/kubernetes/items/local/selenoid-external-service.yaml` (selenoid-external)


##### 11. Init kubernetes

`$ kubectl apply -f deployment/kubernetes/items/local/selenoid.yaml`

    service "selenoid" configured
    deployment "selenoid" configured
    service "selenoid-ui" configured
    deployment "selenoid-ui" configured


`$ kubectl apply -f deployment/kubernetes/items/local/selenoid-external-service.yaml`

    service "selenoid-external" configured
    service "selenoid-ui-external" configured


`$ kubectl get pods`

`NAME`|`READY`|`STATUS`|`RESTARTS`|`AGE`
------|-------|--------|----------|-----
`selenoid-5c674fb76b-jjdst`|      `1/1`|       `Running`|   `2`|          `2h`
`selenoid-ui-5dd9cbdd6c-9mfgr`|   `1/1`|       `Running`|   `2`|         `2h`

## View VNC session

To see VNC session go to `http://MINIKUBE_IP:NODE_PORT/` 

The `NODE_PORT` defined in `deployment/kubernetes/items/local/selenoid-external-service.yaml` (selenoid-ui-external)


## Running the tests

Run tests via IntelliJ IDEA or 

`$ mvn clean test`

## Deployment

`$ kubectl create -f deployment/kubernetes/items/local/tests.yaml`
To see output:
`$ kubectl get pods`

`NAME`|`READY`|`STATUS`|`RESTARTS`|`AGE`
------|-------|--------|----------|-----
`run-tests-82xlv`|          `1/1`|       `Running`|   `0`|          `5s`
`selenoid-5c674fb76b-jjdst`|      `1/1`|       `Running`|   `0`|          `2h`
`selenoid-ui-5dd9cbdd6c-9mfgr`|   `1/1`|       `Running`|   `11`|         `2h`

`$ kubectl logs run-tests-82xlv -f`

## Authors

Roman Kokhatskyi


## Troubleshooting

#### Browser doesn't show anything for VNC session (This site can't be reached)

To solve this issue you need to do next:

1\. *Remove all jobs for kubectl*

`$ kubectl delete jobs --all`

2\. *Stop minikube service*

`$ minikube stop`

3\. *Clear DNS cache*

####### Windows

`$ ipconfig /flushdns`

####### Mac OS

`$ sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder`


####### Linux

`$ sudo service nscd restart`

4\. *Start minukube service*

`$ minikube start`

5\. *Init kubernetes again*

Repeat Init kubernetes step


#### Job don't start on minikube

To get error detail:

`$ kubectl describe pod <job name>`