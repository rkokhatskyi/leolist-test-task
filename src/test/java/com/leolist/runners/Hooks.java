package com.leolist.runners;

import com.leolist.base.DriverManager;
import com.leolist.reporting.AllureReporter;
import com.leolist.reporting.EventLogger;
import com.leolist.reporting.FileAppender;
import com.leolist.tests.TestContext;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.Dimension;

import static com.leolist.base.DriverManager.driver;
import static com.leolist.utils.Properties.getBrowserDimension;

public class Hooks extends TestRunner {

    /**
     * Performed before each test scenario.
     */
    @Before(order = 0)
    public void prepareData(Scenario scenario) {

        //run browser
        DriverManager.setUpBrowser();
        TestContext.clear();

        String output = "\n==============================" +
                "\nStarting - " + scenario.getName() +
                "\n------------------------------\n";
        System.out.println(output);

        EventLogger.info(output);
    }

    /**
     * Performed after each test scenario.
     */
    @After(order = 0)
    public void clearData(Scenario scenario) {
        if (scenario.isFailed()) {
            FileAppender.addFailedResult(scenario.getName());
            AllureReporter.attachScreenshot(scenario);
        }

        String output = "Finished - " + scenario.getName() +
                "\nStatus - " + scenario.getStatus() +
                "\nTags - " + scenario.getSourceTagNames() +
                "\n==============================\n\n";
        System.out.println(output);

        FileAppender.addResult(output);
        EventLogger.info(output);

        EventLogger.saveSeleniumLogs();
        DriverManager.stopDriver();
    }

    /**
     * Increases screen resolution before tests execution.
     */
    @Before("@expandedDisplay")
    public void expandDisplay() {
        driver().manage().window().setSize(new Dimension(3840, 2160));
    }

    /**
     * Resets display settings after test execution.
     */
    @After("@expandedDisplay")
    public void resetDisplaySettings() {
        driver().manage().window().setSize(getBrowserDimension());
    }
}