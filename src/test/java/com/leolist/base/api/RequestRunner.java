package com.leolist.base.api;

import com.leolist.base.TestNGRunner;
import com.leolist.reporting.EventLogger;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matcher;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static io.restassured.RestAssured.given;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.Matchers.is;

/**
 * Class for running requests and getting results
 */
public class RequestRunner {

    private static ByteArrayOutputStream requestStream;
    private static ByteArrayOutputStream responseStream;

    /**
     * @return - RequestSpecification with given parameters (preconditions for requests)
     */
    private static RequestSpecification addParameters() {

        requestStream = new ByteArrayOutputStream();
        responseStream = new ByteArrayOutputStream();

        return given().filters(
                new RequestLoggingFilter(LogDetail.ALL, new PrintStream(requestStream)),
                new ResponseLoggingFilter(LogDetail.ALL, new PrintStream(responseStream)));
    }

    /**
     * @param partialRequest - part of GET request after /api/qa/ in link
     */
    public static void executeGetRequest(String partialRequest) {
        Response response = addParameters().get(partialRequest);

        EventLogger.logRequest(requestStream);
        EventLogger.logResponse(responseStream);

        validateResponseStatusCode(response.getStatusCode(), is(HTTP_OK));
    }

    private static void validateResponseStatusCode(Integer statusCode, Matcher matcher) {
        if (!matcher.matches(statusCode)) {
            TestNGRunner.skipTest(
                    "Wrong status code for API response." +
                            "\nActual Status code is <" + statusCode + ">" +
                            "\nExpected status code " + matcher);
        }
    }
}