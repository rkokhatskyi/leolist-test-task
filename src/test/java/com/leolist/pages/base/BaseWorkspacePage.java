package com.leolist.pages.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import static com.leolist.base.DriverManager.driver;

public class BaseWorkspacePage {

    @FindBy(tagName = "header")
    protected WebElement header;

    @FindBy(tagName = "footer")
    protected WebElement footer;

    @FindBy(id = "login-link")
    protected WebElement loginLink;

    @FindBy(id = "register-link")
    protected WebElement signupLink;

    @FindBy(id = "(//i[@class='icon-question-o'])[1]")
    protected WebElement knowledgeBaseButton;

    @FindBy(id = "(//i[@class='icon-heart active'])[1]")
    protected WebElement favoritesButton;

    @FindBy(linkText = "Post an Ad")
    protected WebElement postOnAdButton;

    @FindBy(linkText = "Ads")
    protected WebElement adsButton;

    @FindBy(xpath = "//a[@href='/directory/calgary']")
    protected WebElement directoryLink;

    @FindBy(xpath = "//div[@class='logo pull-left']")
    protected WebElement logo;

    protected BaseWorkspacePage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver())), this);
    }
}