package com.leolist.tests;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines test context to allow using the same entity through steps.
 */
public class TestContext {

    /**
     * Clears test context.
     */
    public static void clear() {
        sharedVariables.clear();
    }

    private static final Map<String, Object> sharedVariables = new HashMap<>();

    public static Object getSharedVariable(String key) {
        return sharedVariables.get(key);
    }

    public static void setSharedVariable(String key, Object value) {
        sharedVariables.put(key, value);
    }
}