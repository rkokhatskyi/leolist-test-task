FROM java:8-jdk

MAINTAINER roman.kokhatskyi@gmail.com

# install packages
RUN apt-get update && \

    # Download and install maven 3.5.3 version
    curl -O http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz && \
    tar xf apache-maven-3.5.4-bin.tar.gz -C /usr/bin && \
    cd /usr/bin && \
    ln -s apache-maven-3.5.4 maven && \

    apt-get install -y \
        nano \
        sudo

#append files
ADD . /var/qa

RUN rm -rf /var/qa/deployment

RUN cd /var/qa && \

    /usr/bin/maven/bin/mvn clean && \
    /usr/bin/maven/bin/mvn test -Dincludes="**/runners/EmptyTestRunner" && \
    /usr/bin/maven/bin/mvn clean
WORKDIR /var/qa