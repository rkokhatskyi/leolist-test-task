package com.leolist.enums;

/**
 * Config-defined properties enumerator.
 */
public enum PropertiesNames {

    BROWSER_HEIGHT("browser.height"),
    BROWSER_WIDTH("browser.width"),
    HUB("hub"),
    CONFIG_DIR("config.dir"),
    DRIVERS_DIR("drivers.dir"),
    BASE_URL("base.url"),
    REPORT_DIR("report.dir"),
    LOGS_DIR("logs.dir");

    private final String value;

    PropertiesNames(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}