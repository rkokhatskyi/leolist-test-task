package com.leolist.base;

import com.leolist.reporting.DriverEventListener;
import com.leolist.reporting.EventLogger;
import com.leolist.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.util.Strings;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import static com.leolist.base.TestNGRunner.skipTest;

public class DriverManager {
    private static WebDriver driver;
    private static EventFiringWebDriver eventFiringWebDriver;

    public static EventFiringWebDriver driver() {
        return eventFiringWebDriver;
    }

    /**
     * Sets up remote driver based on selected platform and web browser.
     * Notes: Platform and Browser could be specified at configuration file.
     */
    private static RemoteWebDriver setUpRemoteDriver() {

        String url = Properties.getHubUrl();
        Assert.assertNotNull(url, "Cannot setup remote webdriver with no hub URL!");
        Assert.assertNotEquals(url, "", "Cannot setup remote webdriver with empty hub URL!");


        DesiredCapabilities capabilities = BrowserPresets.getChromeDesiredCapabilities();
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", true);
        capabilities.setCapability("seleniumProtocol", "WebDriver");

        try {
            return new CustomRemoteWebDriver(new URL(url), capabilities);
        } catch (MalformedURLException e) {
            skipTest("Cannot setup remote webdriver using incorrect hub URL! " + e.getMessage());
        }

        return null;
    }

    /**
     * Sets up browser before test execution.
     */
    public static void setUpBrowser() {
        String hubUrl = Properties.getHubUrl();

        // if not null and not empty
        if (!Strings.isNullOrEmpty(hubUrl)) {
            driver = setUpRemoteDriver();
        } else {
            driver = setUpChrome();
        }

        Assert.assertNotNull(driver, "Webdriver is not set up!");

        driver.manage().window().setSize(Properties.getBrowserDimension());

        eventFiringWebDriver = new EventFiringWebDriver(driver);
        eventFiringWebDriver.register(new DriverEventListener());
    }


    /**
     * Sets up Chrome web driver.
     */
    private static WebDriver setUpChrome() {
        String chromeDriverPath = Properties.getChromeDriverPath();
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);

        ChromeOptions options = BrowserPresets.getChromePresetOptions();

        return new ChromeDriver(options);
    }


    //closes browser and kills related process
    public static void stopDriver() {

        SessionId sessionId = driver instanceof CustomRemoteWebDriver ?
                ((CustomRemoteWebDriver) driver).getSessionId() :
                ((ChromeDriver) driver).getSessionId();

        if (Objects.nonNull(sessionId)) {
            EventLogger.info("Closing browser. Session id: " + sessionId.toString());
            driver.quit();
        }

        driver = null;
    }
}