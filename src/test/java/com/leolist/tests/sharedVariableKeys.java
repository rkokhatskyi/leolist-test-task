package com.leolist.tests;

/**
 * Contains keys for shared variables, which could be used across step definitions.
 */
public class sharedVariableKeys {

    public final static String selectedCity = "selectedCity";
    public final static String searchKeyword = "searchKeyword";
}