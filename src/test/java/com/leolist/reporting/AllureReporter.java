package com.leolist.reporting;

import com.google.common.base.Strings;
import com.leolist.base.TestNGRunner;
import cucumber.api.Scenario;
import org.openqa.selenium.OutputType;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import static com.leolist.base.DriverManager.driver;

public class AllureReporter {

    /**
     * Method-helper to attach byteArray (screenshots)
     * <p>
     * Also, can be re-used for other byte arrays
     *
     * @param byteArray - byteArray to be attached
     * @param scenario  - Cucumber scenario where attach file
     */
    private static void attachFile(byte[] byteArray, Scenario scenario) {
        String mimeType = getMimeType(byteArray);

        if (Strings.isNullOrEmpty(mimeType)) {
            EventLogger.debug("Mime type is unknown");
        } else {
            EventLogger.debug("Mime type is: " + mimeType);
        }
        scenario.embed(byteArray, mimeType);
    }

    /**
     * @param scenario - Cucumber scenario where attach file
     */
    public static void attachScreenshot(Scenario scenario) {
        byte[] screenshot = (driver()).getScreenshotAs(OutputType.BYTES);
        EventLogger.debug("Screenshot is being attached");
        attachFile(screenshot, scenario);
        EventLogger.debug("Screenshot attached");
    }

    /**
     * @param byteArray - byte array to get mimeType
     * @return String of mime type of the file/byteArray
     */
    private static String getMimeType(byte[] byteArray) {
        String mime = null;
        try (InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(byteArray))) {
            mime = URLConnection.guessContentTypeFromStream(inputStream);
        } catch (IOException exception) {
            TestNGRunner.skipTest("Cannot get mime type for byte array");
            EventLogger.error(exception);
            // NOTE: we don't need to close inputStream as we're using try-with-resources. It closes streams automatically.
        }
        return mime;
    }
}