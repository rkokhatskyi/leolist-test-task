package com.leolist.base;

import org.testng.SkipException;

public class TestNGRunner {

    /**
     * Throws SkipException to prevent running blocked tests.
     *
     * @param message is message to display reason of skipping.
     */
    public static void skipTest(String message) {
        throw new SkipException(message);
    }
}