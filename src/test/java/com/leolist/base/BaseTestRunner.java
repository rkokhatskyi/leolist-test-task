package com.leolist.base;

import com.leolist.reporting.EventLogger;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.runtime.CucumberException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Objects;

/**
 * Implements basic logic for tests solution, including web drivers setup logic and related things.
 */
public class BaseTestRunner {

    private TestNGCucumberRunner testNGCucumberRunner;

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        try {
            testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        } catch (CucumberException exception) {
            EventLogger.error(exception);
            throw new Error("Cannot start Cucumber tests.");
        }
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() {
        if (Objects.nonNull(testNGCucumberRunner)) {
            testNGCucumberRunner.finish();
        }
    }

    @Test(groups = "cucumber", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        EventLogger.info("Running Cucumber feature: " + cucumberFeature.getCucumberFeature().getFeatureElements());
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }
}