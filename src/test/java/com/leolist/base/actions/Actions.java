package com.leolist.base.actions;

import com.leolist.base.DriverManager;
import com.leolist.reporting.EventLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.leolist.base.AdditionalConditions.elementIsNotVisible;
import static com.leolist.base.AdditionalConditions.pageLoaded;

/**
 * Tests actions container, which used for initialization and providing access to all actions.
 * All test actions should be accessible via this implementation for solving concurrent running problems.
 * Contains common for all children actions.
 */
public class Actions {

    /**
     * Default extra small timeout for waiting element visibility.
     */
    private static final int ELEMENT_EXTRASMALL_TIMEOUT_SECONDS = 0;

    /**
     * Default timeout for waiting element changing property
     */
    private static final int ELEMENT_TIMEOUT_SECONDS = 20;

    private static EventFiringWebDriver driver() {
        return DriverManager.driver();
    }

    /**
     * Thread sleeping for specified time.
     */
    public static void wait(int seconds) {
        wait(seconds, 0);
    }

    /**
     * Thread sleeping for specified time.
     */
    private static void wait(int seconds, int milliseconds) {
        try {
            Thread.sleep(seconds * 1000 + milliseconds);
        } catch (Exception e) {
            EventLogger.error(e);
        }
    }

    /**
     * Opens specified URL in browser.
     */
    public static void openPage(String url) {
        driver().navigate().to(url);
    }


    public static void insertIntoField(String value, WebElement textField) {
        driver().executeScript(String.format("arguments[0].value='%s'", value), textField);
    }

    /**
     * Wait for element visibility
     *
     * @param element is web element to wait for visibility
     */
    public static void waitForVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Wait for element visibility.
     *
     * @param element is web element to wait for visibility.
     * @param timeout is timeout in seconds to wait for element.
     */
    private static void waitForVisibility(WebElement element, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver(), timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Wait for element invisibility.
     *
     * @param element is web element to wait for invisibility.
     */
    protected static void waitForInvisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS);
        try {
            wait.until(elementIsNotVisible(element));
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            // Returns true because the element is not present in DOM. The
            // try block checks if the element is present but is invisible.
        }
    }

    /**
     * Wait for element to be clickable.
     *
     * @param selector is element selector to click.
     */
    protected static WebElement waitToBeClickable(By selector) {
        return new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(selector));
    }

    /**
     * Wait for element to be clickable.
     *
     * @param webElement is element to click.
     */
    protected static void waitToBeClickable(WebElement webElement) {
        new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    /**
     * Wait for page loaded
     *
     * @param pagePartialUrl is web page partial url.
     */
    public static void waitForPageLoaded(String pagePartialUrl) {
        WebDriverWait wait = new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS);
        wait.until(pageLoaded(pagePartialUrl));
    }

    /**
     * Wait for checking if the given text is present in the specified element.
     *
     * @param element the WebElement
     * @param text    to be present in the element
     */
    protected static void waitForText(WebElement element, String text) {
        WebDriverWait wait = new WebDriverWait(driver(), ELEMENT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }


    /**
     * Check whether element is presented on page.
     *
     * @param element to check.
     * @return true if element is presented, otherwise false.
     */
    private static boolean isElementPresented(WebElement element) {
        try {
            element.getTagName();
            return true;
        } catch (Exception e) {
            // We are fine with error there as element couldn't be presented. Log on debug level.
            EventLogger.debug(e);
        }
        return false;
    }

    /**
     * Check whether element is visible on the page.
     *
     * @param element is element to check.
     * @return true if element is visible, otherwise false.
     */
    public static boolean isElementVisible(WebElement element) {
        if (!isElementPresented(element)) {
            return false;
        }

        WebDriverWait wait = new WebDriverWait(driver(), ELEMENT_EXTRASMALL_TIMEOUT_SECONDS);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Throwable th) {
            return false;
        }
        return true;
    }
}