package com.leolist.runners;

import com.leolist.base.BaseTestRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

/**
 * Test runner for running tests. Required as per CucumberOptions implementations.
 * Defines basic concerns including testing scope for the single run
 */
@CucumberOptions(
        features = "src/test/java/com/leolist/tests",
        glue = {"com.leolist.stepDefinitions", "com.leolist.runners"},
        tags = {"@test"},
        strict = true,
        snippets = SnippetType.CAMELCASE,
        monochrome = true,
        plugin = "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"
)
public class TestRunner extends BaseTestRunner {
}