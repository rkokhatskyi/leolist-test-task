package com.leolist.base.api;

import com.leolist.base.DriverManager;

public class RequestActions {

    public static void validatePageResponseCode() {
        String currentUrl = DriverManager.driver().getCurrentUrl();
        RequestRunner.executeGetRequest(currentUrl);
    }
}