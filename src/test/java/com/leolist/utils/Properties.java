package com.leolist.utils;

import com.leolist.enums.PropertiesNames;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Dimension;

import java.nio.file.Paths;

public class Properties {

    public static String getBaseUrl() {
        return System.getProperty(PropertiesNames.BASE_URL.toString());
    }

    public static String getConfigDir() {
        return System.getProperty(PropertiesNames.CONFIG_DIR.toString());
    }

    public static String getHubUrl() {
        return System.getProperty(PropertiesNames.HUB.toString());
    }

    public static Dimension getBrowserDimension() {
        int width = Integer.parseInt(System.getProperty(PropertiesNames.BROWSER_WIDTH.toString()));
        int height = Integer.parseInt(System.getProperty(PropertiesNames.BROWSER_HEIGHT.toString()));
        return new Dimension(width, height);
    }

    public static String getReportsDirPath() {
        return System.getProperty(PropertiesNames.REPORT_DIR.toString());
    }

    private static String getDriversDir() {
        return System.getProperty(PropertiesNames.DRIVERS_DIR.toString());
    }

    public static String getLogsDir() {
        return System.getProperty(PropertiesNames.LOGS_DIR.toString());
    }

    public static String getChromeDriverPath() {
        String basePath = getDriversDir();
        if (basePath != null) {
            if (SystemUtils.IS_OS_WINDOWS) {
                return Paths.get(basePath, "chromedriver.exe").toString();
            }
            if (SystemUtils.IS_OS_MAC) {
                return Paths.get(basePath, "chromedriver_mac").toString();
            }
        }
        throw new IllegalStateException("Chrome driver doesn't exist.");
    }
}