package com.leolist.reporting;

import com.leolist.base.api.RequestActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.util.Arrays;

/**
 * Web driver implementation with extending of event listener.
 */
public class DriverEventListener implements WebDriverEventListener {
    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
        EventLogger.debug("Before accepting alert");
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
        EventLogger.debug("After accepting completed");
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
        EventLogger.debug("Alert dismissed");
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
        EventLogger.debug("Dismissing alert...");
    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        EventLogger.debug("Navigating to: ".concat(s));
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        EventLogger.debug("Navigate completed to :".concat(s));
        RequestActions.validatePageResponseCode();
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        EventLogger.debug("Navigating back...");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        EventLogger.debug("Navigate back - completed");
        RequestActions.validatePageResponseCode();
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        EventLogger.debug("Navigating forward...");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        EventLogger.debug("Navigate forward - completed");
        RequestActions.validatePageResponseCode();
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        EventLogger.debug("Refreshing...");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        EventLogger.debug("Refresh completed");
        RequestActions.validatePageResponseCode();
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        EventLogger.debug(String.format("Trying to find element... Locator: %s", by));
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        EventLogger.debug(String.format("Element found: %s. Locator: %s", webDriver, by));
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        EventLogger.debug(String.format("Trying to click on the element: %s", webElement));
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        EventLogger.debug(String.format("Click on the element completed. Element: %s", webElement));
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        EventLogger.debug(String.format("Changing value for element: %s. New value: %s", webElement, Arrays.toString(charSequences)));
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        EventLogger.debug(String.format("Value updated to: %s", Arrays.toString(charSequences)));
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        EventLogger.debug("Executing script: ".concat(s));
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        EventLogger.debug("Executing completed.");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        EventLogger.error(throwable.getMessage());
    }
}
