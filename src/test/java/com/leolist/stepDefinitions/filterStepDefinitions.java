package com.leolist.stepDefinitions;

import com.leolist.pages.SearchPage;
import com.leolist.tests.TestContext;
import com.leolist.tests.sharedVariableKeys;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class filterStepDefinitions {

    private final SearchPage searchPage = new SearchPage();

    @When("^user selects the \"([^\"]*)\" region$")
    public void userSelectsTheRegion(String region) {
        searchPage.filter.selectRegion(region);
    }

    @And("^user selects the \"([^\"]*)\" district$")
    public void userSelectsTheDistrict(String district) {
        searchPage.filter.selectDistrict(district);
    }

    @When("^user clicks More filter button$")
    public void userClicksMoreFilterButton() {
        searchPage.filter.clickMoreFiltersButton();
    }

    @And("^user selects Has Photos checkbox$")
    public void userSelectsHasPhotosCheckbox() {
        searchPage.filter.checkHasPhotosCheckbox();
    }

    @And("^user inserts \"([^\"]*)\" text into Search input$")
    public void userInsertsTextIntoSearchInput(String text) {
        searchPage.filter.insertIntoSearchKeywordField(text);
        TestContext.setSharedVariable(sharedVariableKeys.searchKeyword, text);
    }

    @And("^user clicks Show Ads button$")
    public void userClicksShowAdsButton() {
        searchPage.filter.clickShowAdsButton();
    }

    @And("^user selects \"([^\"]*)\" check box in Cities filter$")
    public void userSelectsCheckBoxInCitiesFilter(String city) {
        searchPage.filter.selectCity(city);
        TestContext.setSharedVariable(sharedVariableKeys.selectedCity, city);
    }
}