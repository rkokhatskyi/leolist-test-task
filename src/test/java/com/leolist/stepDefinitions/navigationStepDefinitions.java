package com.leolist.stepDefinitions;

import com.leolist.base.actions.Actions;
import com.leolist.pages.CalgaryPage;
import com.leolist.utils.Properties;
import cucumber.api.java.en.Given;
import org.junit.Assert;

public class navigationStepDefinitions {

    @Given("^user is on Calgary page$")
    public void userIsOnCalgaryPage() {
        String baseUrl = Properties.getBaseUrl();
        Actions.openPage(baseUrl);

        CalgaryPage calgaryPage = new CalgaryPage();

        // assert title and URL for the page
        Assert.assertTrue(calgaryPage.isPageDisplayed());
    }
}