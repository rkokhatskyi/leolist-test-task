package com.leolist.pages.components;

import com.leolist.base.DriverManager;
import com.leolist.base.TestNGRunner;
import com.leolist.base.actions.Actions;
import com.leolist.pages.base.BaseWorkspacePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.List;

public class filter extends BaseWorkspacePage {

    @FindBy(id = "more-filters")
    private WebElement moreFiltersButton;

    @FindBy(xpath = "//a[@class='form-control filter']")
    private List<WebElement> dropDownFilters;

    @FindBy(id = "search-q")
    private WebElement searchKeywordField;

    @FindBy(xpath = "//button[@data-view='list-view']")
    private WebElement listViewButton;

    @FindBy(xpath = "//button[@data-view='block-view']")
    private WebElement gridViewButton;

    @FindBy(xpath = "//label[@for='has_photos']")
    private WebElement hasPhotosCheckbox;

    @FindBy(xpath = "//button[text()='Show Ads']")
    private WebElement showAdsButton;

    private EventFiringWebDriver driver = DriverManager.driver();

    public filter() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
    }

    public void clickMoreFiltersButton() {
        moreFiltersButton.click();
    }

    public void checkHasPhotosCheckbox() {
        Actions.waitForVisibility(hasPhotosCheckbox);
        CheckBox checkBox = new CheckBox(hasPhotosCheckbox);
        if (checkBox.isSelected()) {
            TestNGRunner.skipTest("Checkbox is already selected");
        }

        checkBox.select();
    }

    public void insertIntoSearchKeywordField(String value) {

        // We can just use sendKeys method, but there was a requirement to insert into field
        Actions.insertIntoField(value, searchKeywordField);
    }

    private void expandRegionDropDown() {
        if (dropDownFilters.isEmpty()) {
            TestNGRunner.skipTest("Cannot find dropdown filters");
        }

        dropDownFilters.get(0).click();
    }

    public void selectRegion(String region) {
        expandRegionDropDown();
        WebElement element = driver.findElement(By.xpath(String.format("//*[@id='overview']//div[contains(text(),'%s')]", region)));
        element.click();
    }

    public void selectDistrict(String district) {
        WebElement element = driver.findElement(By.xpath(String.format("//a[text() = '%s']", district)));
        element.click();
    }

    public void clickShowAdsButton() {
        showAdsButton.click();
    }

    public void selectCity(String city) {
        WebElement element = driver.findElement(By.xpath(String.format("//label[text()='%s']", city)));

        CheckBox checkBox = new CheckBox(element);

        if (checkBox.isSelected()) {
            TestNGRunner.skipTest("Checkbox is already selected");
        }

        checkBox.select();
    }
}